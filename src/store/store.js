import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  token: '',
  expiration: '',
  authenticatedUser: '',
  isAuth: ''
}

const getters = {
  isAuthenticated: (state, getters) => {
    // if (state.isAuth) return true
    // else return false
    if (getters.getToken) return true
    else return false
  },
  getToken: (state, getters) => {
    let token = localStorage.getItem('token')
    let expiration = localStorage.getItem('expiration')

    if (!token || !expiration) { return null } else { return token }

    if (Date.now() > parseInt(expiration)) {
      getters.destroyToken()
    } else {
      return token
    }
  }
}

const actions = {}

const mutations = {
  setToken (state, data) {
    let token = data.token
    let expiration = data.expiration

    localStorage.setItem('token', token)
    localStorage.setItem('expiration', expiration)

    state.isAuth = true
  },
  destroyToken (state) {
    localStorage.removeItem('token')
    localStorage.removeItem('expiration')

    state.isAuth = null
  },
  setAuthenticatedUser (state, user) {
    state.authenticatedUser = user
  }
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
