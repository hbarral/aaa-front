import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VeeValidate from 'vee-validate'
import store from './store/store.js'

Vue.use(VueAxios, axios)
Vue.use(VeeValidate)

// Axios defaults
axios.defaults.baseURL = 'http://127.0.0.1:8000'
axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.getters.getToken

router.beforeEach(
  (to, from, next) => {
    if (to.matched.some(record => record.meta.forVisitors)) {
      if (store.getters.isAuthenticated) {
        next({
          path: '/contacts'
        })
      } else next()
    } else if (to.matched.some(record => record.meta.forAuth)) {
      if (!store.getters.isAuthenticated) {
        next({
          path: '/login'
        })
      } else next()
    } else next()
  }
)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
