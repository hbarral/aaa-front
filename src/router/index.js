import Vue from 'vue'
import Router from 'vue-router'
import Contacts from '@/components/Contacts'
import Login from '@/components/authentication/Login'
import Home from '../components/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        forVisitors: true
      }
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: Contacts,
      meta: {
        forAuth: true
      }
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Home,
      meta: {
        forAuth: true
      }
    }
  ],
  linkActiveClass: 'active'
})
